# Videoplayer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

The purpose of this video player is to be a frontend to the [DebConf Video
archive](https://meetings-archive.debian.net/pub/debian-meetings). A
design feature of it is that it does not require *any* server-side
component; everything is done on the client side.

Note that this is very much still in development, and not entirely ready
for prime time yet.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
