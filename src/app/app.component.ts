import { Component } from '@angular/core';
import { MetadataService } from './metadata.service';
import { Conference } from './conference';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DebConf Video Player';

  get videoSlug(): string {
    let video = this.metadataService.selectedVideo;
    if(video === undefined) {
      return undefined;
    }
    return video._slug;
  }

  get selectedConference(): Conference {
    return this.metadataService.selectedConference;
  }

  constructor(private metadataService: MetadataService) {}
}
