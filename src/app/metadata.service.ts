import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Conference, VideoData } from './conference';
import { FileList } from './fileslist';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MetadataService {
  metadataUrl = 'https://debconf-video-team.pages.debian.net/archive-meta/';
  indexUrl = 'index.json';
  conferences: { [file: string]: Conference } = {};
  filelist: FileList = { files: [] };

  get selectedConference(): Conference | undefined {
    let route = this.route.routerState.snapshot.root.firstChild;
    if(!route) {
      return undefined;
    }
    const year = route.paramMap.get('year');
    const base = route.paramMap.get('basename');
    return this.conferences[year + '/' + base + '.json'];
  }

  get selectedVideo(): VideoData | undefined {
    if(this.selectedConference === undefined) {
      return undefined;
    }
    let route = this.route.routerState.snapshot.root.firstChild;
    let slug = route.queryParamMap.get('video');
    for(let video of this.selectedConference.videos) {
      if(video._slug === slug) {
        return video;
      }
    }
    return undefined;
  }

  constructor(private http: HttpClient, private route: Router) { }

  getFileList(): Observable<FileList> {
    let rv = this.http.get<FileList>(this.metadataUrl + this.indexUrl);
    return rv;
  }

  getConference(file: string): Observable<Conference> {
    if(!(file in this.conferences)) {
      let conf = this.http.get<Conference>(this.metadataUrl + file);
      conf.subscribe((conf) => {
        for(let video of conf.videos) {
          video._slug = video.video.replace('/', '-');
        }
        conf._file = file;
        this.conferences[file] = conf
      });
      return conf;
    }
    return of(this.conferences[file]);
  }

  populate(): void {
    this.getFileList().subscribe(filelist => {
      this.filelist = filelist;
      for(let file of filelist.files) {
        this.getConference(file);
      }
    });
  }
}
