import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'newlineConvertor'
})
export class NewlineConvertorPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) {}
  transform(value: any): any {
    var rv = value.replace(/\n\n/g, '<br>');
    return this.sanitized.bypassSecurityTrustHtml(rv);
  }

}
