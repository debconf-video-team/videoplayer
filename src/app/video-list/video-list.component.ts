import { Component, OnInit } from '@angular/core';
import { VideoData, Conference } from '../conference';
import { MetadataService } from '../metadata.service';
import { Observable, timer } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  filterString: string = '';
  filterProperties: { [type: string]: boolean} = {};
  files: Array<string> = [];

  get selectedVideo(): VideoData {
    return this.metadataService.selectedVideo;
  }
  get selectedConference(): Conference {
    return this.metadataService.selectedConference;
  }
  get haveFilter(): boolean {
    for(let property in this.filterProperties) {
      if(this.filterProperties[property]) {
        return true;
      }
    }
    return false;
  }

  get videos(): Array<VideoData> {
    let rv: Array<VideoData> = [];
    let c: {[name: string]:Conference};
    if(this.selectedConference !== undefined) {
      c = {};
      c["i"] = this.selectedConference;
    } else {
      c = this.metadataService.conferences;
    }
    for (let conference in c) {
      if(this.filterString === undefined || this.filterString === '' || !this.haveFilter) {
        for(let video of c[conference].videos) {
          rv.push(video);
        }
      } else {
        for(let video of c[conference].videos) {
	  const filter = this.filterString.toLowerCase();
          if(this.filterProperties["title"] && video.title.toLowerCase().includes(filter)) {
            rv.push(video);
            continue;
          }
          if(this.filterProperties["description"] && video.description && video.description.toLowerCase().includes(filter)) {
            rv.push(video);
            continue;
          }
          if(this.filterProperties["speaker"] && video.speakers) {
            let found = false;
            for(let speaker of video.speakers) {
              if(speaker.toLowerCase().includes(filter)) {
                found = true;
                break;
              }
            }
            if(found) {
              rv.push(video);
              continue;
            }
          }
        }
      }
    }
    return rv;
  }
  get speakers(): Array<string> {
    if(!this.selectedConference) {
      return [];
    }
    let rv = [];
    for(let video of this.selectedConference.videos) {
      if("speakers" in video) {
	for(let speaker of video.speakers) {
	  rv.push({name: speaker, video: video});
	}
      }
    }
    return rv;
  }
  speakerstring(vid: VideoData): string {
    let comma = '';
    let rv = '';
    for(let speaker of vid.speakers) {
      rv += comma + speaker;
      comma = ', ';
    }
    return rv;
  }

  constructor(private route: ActivatedRoute, private metadataService: MetadataService) { }

  ngOnInit() {
  }
}
