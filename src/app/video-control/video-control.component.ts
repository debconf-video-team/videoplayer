import { Input, Component, OnChanges, SimpleChanges } from '@angular/core';
import { Conference, VideoData, VideoFormat } from '../conference';
import { MetadataService } from '../metadata.service';
import { timer } from 'rxjs';
import { Router } from '@angular/router';

class VideoFile {
  src: string;
  type?: string;
  title?: string;
}

class TrackData {
  src: string;
  kind: string;
  lang: string;
  language: string;
}

@Component({
  selector: 'video-control',
  templateUrl: './video-control.component.html',
  styleUrls: ['./video-control.component.css']
})
export class VideoControlComponent implements OnChanges {
  show: boolean = false;
  @Input() slug: string = "";
  get selectedVideo(): VideoData {
    return this.metadataService.selectedVideo;
  }
  get selectedConference(): Conference {
    return this.metadataService.selectedConference;
  }

  get videoTitle(): string {
    let rv;
    if(this.selectedVideo.speakers !== undefined) {
      for(let speaker of this.selectedVideo.speakers) {
        if(rv === undefined) {
          rv = speaker;
        } else {
          rv += ', ' + speaker;
        }
      }
      rv += ': ' + this.selectedVideo.title;
    } else {
      rv = this.selectedVideo.title;
    }
    return rv;
  }
  htmlVideoType(format: VideoFormat): string {
    if(!format.vcodec && format.acodec === "vorbis" && format.container === "ogg") {
      return "audio/ogg; codec='vorbis'";
    }
    if(!format.vcodec && format.acodec === "opus" && format.container === "ogg") {
      return "audio/ogg; codec='opus'";
    }
    if(format.vcodec === "vp8" && format.acodec === "vorbis") {
      if(format.container === "webm") {
        return "video/webm; codec='vp8, vorbis'";
      }
      if(format.container === "ogg") {
        return "video/ogg; codec='vp8, vorbis'";
      }
    }
    if(format.vcodec === "vp9" && format.acodec === "vorbis") {
      return "video/webm; codec='vp9, vorbis'";
    }
    if(format.vcodec === "vp9" && format.acodec === "opus") {
      return "video/webm; codec='vp9, opus'";
    }
    if(format.vcodec === "theora" && format.acodec === "vorbis" && format.container === "ogg") {
      return "video/ogg; codec='theora, vorbis'";
    }
    if(format.vcodec === "mpeg4" && format.container === "mp4") {
      return "video/mp4";
    }
    if(format.vcodec === "av1" && format.acodec === "opus") {
      return "video/webm; codec='av1, opus'";
    }
    return undefined;
  }
  get htmlTracks(): Array<TrackData> {
    if(!this.selectedVideo || !this.selectedVideo.subtitles) {
      return [];
    }
    let st = this.selectedVideo.subtitles;
    let rv = [];
    let trans = {
      eng: {short: "en", long: "English"},
      jpn: {short: "ja", long: "Japanese"},
      fra: {short: "fr", long: "French"},
    }
    for(let sublang in st) {
      let src = st[sublang];
      if(src.substring(src.length - 3, src.length) !== "vtt") {
        continue;
      }
      let kind="captions";
      if(sublang !== this.selectedVideo.language) {
        kind="subtitles";
      }
      rv.push({lang: trans[sublang].short, language: trans[sublang].long, kind: kind, src: st[sublang]});
    }
    return rv;
  }
  get slideUrls(): Array<VideoFile> {
    if(!("slides" in this.selectedVideo)) {
      return [];
    }
    let rv = [];
    for(let slide of this.selectedVideo.slides) {
      if(slide.match(/^http/)) {
        rv.push({src: slide, title: slide});
      } else {
        rv.push({src: this.selectedConference.conference.video_base + '/' + slide, title: slide});
      }
    }
    console.log(rv);
    return rv;
  }
  get startTime(): number {
    let route = this.route.routerState.snapshot.root.firstChild;
    let time = +route.queryParamMap.get('start');
    return time;
  }
  get htmlVideoFiles(): Array<VideoFile> {
    if(!this.selectedConference || this.selectedConference.conference.video_formats === undefined) {
      return [];
    }
    let rv: Array<VideoFile> = [];
    let htmltype = this.htmlVideoType(this.selectedConference.conference.video_formats["default"]);
    if(htmltype !== undefined) {
      rv.push({src: this.selectedConference.conference.video_base + '/' + this.selectedVideo.video, type: htmltype});
    }
    for(let format in this.selectedVideo.alt_formats) {
      htmltype = this.htmlVideoType(this.selectedConference.conference.video_formats[format]);
      if(htmltype !== undefined) {
        rv.push({src: this.selectedConference.conference.video_base + '/' + this.selectedVideo.alt_formats[format], type: htmltype});
      }
    }
    return rv;
  }
  videoDescription(vf: VideoFormat): string {
    let type = '';
    let desc = undefined;
    if(vf.vcodec) {
      type += vf.vcodec;
      if(vf.acodec) {
        type += '/';
      }
      desc = 'video recording';
    }
    if(vf.acodec) {
      type += vf.acodec;
      if(!desc) {
        desc = 'audio recording';
      }
    }
    if(vf.bitrate) {
      type += ', ' + vf.bitrate + 'bps';
    }
    return desc + ' (' + type + ')';
  }
  get videoFiles(): Array<VideoFile> {
    let rv: Array<VideoFile> = [];
    if(!this.selectedConference.conference.video_formats) {
      return [{src: this.selectedConference.conference.video_base + '/' + this.selectedVideo.video, type: "recording, unknown codecs"}];
    }
    let vf = this.selectedConference.conference.video_formats;
    let vb = this.selectedConference.conference.video_base;
    rv.push({src: vb + '/' + this.selectedVideo.video, type: 'default ' + this.videoDescription(vf["default"])});
    for(let format in this.selectedVideo.alt_formats) {
      rv.push({src: vb + '/' + this.selectedVideo.alt_formats[format], type: '"' + format + '" ' + this.videoDescription(vf[format])});
    }
    return rv;
  }

  constructor(private metadataService: MetadataService, private route: Router) { }

  ngOnChanges(changes: SimpleChanges) {
    if('slug' in changes) {
      this.show = false;
      timer(100).subscribe(() => {
        if(this.metadataService.selectedVideo !== undefined) {
          this.show = true;
        }
      });
    }
  }
}
