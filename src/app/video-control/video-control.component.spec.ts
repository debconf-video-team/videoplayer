import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewlineConvertorPipe } from '../newline-convertor.pipe';
import { VideoControlComponent } from './video-control.component';
import { MetadataService } from '../metadata.service';
import { defer } from 'rxjs';
import { Conference } from '../conference';
import { FileList } from '../fileslist';
import { MatVideoModule } from 'mat-video';
import { RouterTestingModule } from '@angular/router/testing';

function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('VideoControlComponent', () => {
  let component: VideoControlComponent;
  let fixture: ComponentFixture<VideoControlComponent>;
  const conf: Conference = {
    conference: {
      title: 'test conf',
      series: 'test series',
      edition: 'test edition',
      location: 'test location',
      date: [
        new Date('2019-06-29'),
        new Date('2019-06-30')
      ],
      website: 'test website',
      schedule: 'test schedule',
      video_base: 'test base',
      video_formats: {
        "lq": {
          bitrate: '1024k',
          acodec: 'vorbis'
        }
      },
      comment: 'test comment'
    },
    videos: [
      {
        title: 'test',
        speakers: [ 'test speaker'],
        description: 'test desc',
        details_url: 'test url',
        room: 'test room',
        start: new Date('2019-06-29T11:00:00Z'),
        end: new Date('2019-06-29T12:00:00Z'),
        video: 'test video'
      }
    ]
  };
  const list: FileList = { files: [ 'foo.json' ] };

  beforeEach(async(() => {
    let metadataServiceSpy = jasmine.createSpyObj('MetadataService', ['getFileList', 'getConference']);
    metadataServiceSpy.getFileList.and.returnValue(asyncData(list));
    metadataServiceSpy.getConference.and.returnValue(asyncData(conf));
    TestBed.configureTestingModule({
      declarations: [ VideoControlComponent, NewlineConvertorPipe ],
      providers: [ { provide: MetadataService, useValue: metadataServiceSpy } ],
      imports: [ MatVideoModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
