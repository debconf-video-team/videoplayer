import { Component, OnInit } from '@angular/core';
import { MetadataService } from '../metadata.service';
import { Conference } from '../conference';
import { FileList } from '../fileslist';

@Component({
  selector: 'current-event',
  templateUrl: './current-event.component.html',
  styleUrls: ['./current-event.component.css']
})
export class CurrentEventComponent implements OnInit {
  _files: Array<string> = [];
  get files(): Array<string> {
    if(!this._files.length) {
      this._files = this.metadataService.filelist.files.reverse();
    }
    return this._files;
  }
  get selectedConference(): Conference {
    return this.metadataService.selectedConference;
  }
  get conferences(): { [file: string]: Conference } {
    return this.metadataService.conferences;
  }

  constructor(private metadataService: MetadataService) { }

  ngOnInit() {
    this.metadataService.populate();
  }
}
