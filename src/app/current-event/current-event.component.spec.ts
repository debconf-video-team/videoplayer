import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from '../material-module';
import { MetadataService } from '../metadata.service';
import { MockMetadataService } from '../metadata.mock.service';

import { CurrentEventComponent } from './current-event.component';

describe('CurrentEventComponent', () => {
  let component: CurrentEventComponent;
  let fixture: ComponentFixture<CurrentEventComponent>;

  beforeEach(async(() => {
    let mockService = jasmine.createSpyObj('MetadataService', ['getFileList', 'getConference']);
    new MockMetadataService().confMockMetadataService(mockService);
    TestBed.configureTestingModule({
      declarations: [ CurrentEventComponent ],
      providers: [ { provide: MetadataService, useValue: mockService } ],
      imports: [ MaterialModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
