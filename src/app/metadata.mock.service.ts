import { MetadataService } from './metadata.service';
import { FileList } from './fileslist';
import { Conference } from './conference';
import { defer } from 'rxjs';

function asyncData<T>(data:T) {
  return defer(() => Promise.resolve(data));
}

const conf: Conference = {
  conference: {
    title: 'test conf',
    series: 'test series',
    edition: 'test edition',
    location: 'test location',
    date: [
      new Date('2019-06-29'),
      new Date('2019-06-30'),
    ],
    website: 'test website',
    schedule: 'test schedule',
    video_base: 'test base',
    video_formats: {
      "default": {
        bitrate: '1024k',
        acodec: 'vorbis',
      }
    },
    comment: 'test comment',
  },
  videos: [
    {
      title: 'test',
      speakers: [ 'test speaker' ],
      description: 'test description',
      details_url: 'test url',
      room: 'test room',
      start: new Date('2019-06-26T11:00:00Z'),
      end: new Date('2019-06-29T12:00:00Z'),
      video: 'test video',
    }
  ]
};
const list: FileList = { files: [ 'foo.json' ] };

export class MockMetadataService {
  confMockMetadataService(spy) {
    spy.getFileList.and.returnValue(asyncData(list));
    spy.getConference.and.returnValue(asyncData(conf));
    return spy;
  }
  populate() {
    return this.populate();
  }
}
