import { SecurityContext } from '@angular/core';
import { NewlineConvertorPipe } from './newline-convertor.pipe';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

class MockDomSanitizer implements DomSanitizer {
  sanitize(context: SecurityContext, value: string) : string { return value}
  bypassSecurityTrustHtml(v: string): SafeHtml {return v};
  bypassSecurityTrustStyle(v: string): SafeStyle {return v};
  bypassSecurityTrustScript(v: string): SafeScript {return v};
  bypassSecurityTrustUrl(v: string): SafeUrl {return v};
  bypassSecurityTrustResourceUrl(v: string): SafeResourceUrl {return v};
}

describe('NewlineConvertorPipe', () => {
  it('create an instance', () => {
    const pipe = new NewlineConvertorPipe(new MockDomSanitizer());
    expect(pipe).toBeTruthy();
  });
});
