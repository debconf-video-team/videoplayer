import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { FileList } from './fileslist';
import { Conference } from './conference';
import { RouterTestingModule } from '@angular/router/testing';
import { MetadataService } from './metadata.service';

describe('MetadataService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let metadataService: MetadataService;
  let FileListUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule ],
    })

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    metadataService = TestBed.get(MetadataService);
    FileListUrl = Array(metadataService.metadataUrl, metadataService.indexUrl).join('');
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('#getFileList', () => {
    let expectedFiles: FileList;

    beforeEach(() => {
      expectedFiles = {
        files: [ "a.json", "b.json" ]
      } as FileList;
    });

    it('should return a file list (called once)', () => {
      metadataService.getFileList().subscribe(
        filelist => expect(filelist).toEqual(expectedFiles, 'should return expected files'),
        fail
      );
      const req = httpTestingController.expectOne(FileListUrl);
      req.flush(expectedFiles);
    });

    it('should be OK returning no files', () => {
      metadataService.getFileList().subscribe(
        filelist => expect(filelist).toEqual({files:[]}),
        fail
      );
      const req = httpTestingController.expectOne(FileListUrl);
      req.flush({files:[]});
    });

  });
});
