import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from './material-module';

@Component({ selector: 'video-control', template: "<p>control</p>"})
class TestVideoControlComponent {}
@Component({ selector: 'video-list', template: "<p>list</p>"})
class TestVideoListComponent{}
@Component({ selector: 'current-event', template: "<p>current event</p>"})
class TestCurrentEventComponent{}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [  RouterTestingModule.withRoutes([{path:'',component:TestVideoListComponent},{path:'event/:event',component:TestVideoListComponent}]), MaterialModule ],
      declarations: [
        AppComponent, TestVideoControlComponent, TestVideoListComponent, TestCurrentEventComponent,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'DebConf Video Player'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('DebConf Video Player');
  });

  it('should render title in a span tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('span').textContent).toContain('DebConf Video Player');
  });
});
