export class VideoFormat {
  resolution?: string;
  bitrate: string;
  container?: string;
  vcodec?: string;
  acodec: string;
}
export class ConferenceData {
  title: string;
  series: string;
  edition: string;
  location: string;
  date: Array<Date>;
  website: string;
  schedule: string;
  video_base: string;
  video_formats: { [id: string]: VideoFormat };
  comment?: string;
}
export class VideoData {
  title: string;
  speakers: Array<string>;
  description: string;
  details_url: string;
  room: string;
  start: Date;
  end: Date;
  video: string;
  alt_formats?: { [id: string]: string };
  subtitles?: { [lang: string]:string };
  slides?: Array<string>;
  files?: Array<string>;
  language?: string;
  'non-free'?: string;
  _slug?: string;
}
export class Conference {
  conference: ConferenceData;
  videos: Array<VideoData>;
  _file?: string;
}
